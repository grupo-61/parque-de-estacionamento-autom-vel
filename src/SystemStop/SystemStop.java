package SystemStop;

/**
 * Class representing that system as stop
 *
 * @author joaop
 */
public class SystemStop {

    /**
     * Primitive that represented system stopped or not
     */
    private boolean stop;

    /**
     * Constructor Method
     */
    public SystemStop() {
        this.stop = false;
    }

    /**
     * Method that return if is stopped or not
     *
     * @return the stop
     */
    public boolean isStop() {
        return stop;
    }

    /**
     * Method that sets if is stopped or not
     *
     * @param stop the stop to set
     */
    public void setStop(boolean stop) {
        this.stop = stop;
    }
}
