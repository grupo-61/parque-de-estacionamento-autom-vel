package SystemStop;

/**
 * Thread to stop the System
 *
 * @author joaop
 */
public class ThreadSystemStop<T> implements Runnable {

    /**
     * object that representation of a system being stop
     */
    private SystemStop stop;

    /**
     * Constructor method
     * @param stop 
     */
    public ThreadSystemStop(SystemStop stop) {
        this.stop = stop;
    }

    /**
     * Runnable of Thread
     */
    @Override
    public void run() {
        stop.setStop(!stop.isStop());
    }

}
