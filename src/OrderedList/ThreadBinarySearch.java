package OrderedList;

import SharedObjects.SharedFound;
import java.util.concurrent.Semaphore;

/**
 *
 * @author 8180149
 * @param <T>
 */
public class ThreadBinarySearch<T> implements Runnable {

    private final int SearchStack = 5;
    private OrderedList list;
    private T[] elements;
    private final SharedFound found;
    private final T target;
    private final Semaphore sem;
    private final int semTam;

    /**
     * Constructor Thread that uses recursive method to search for target
     *
     * @param list Ordered list Organizer class
     * @param target element that gonna be search
     * @param found
     */
    public ThreadBinarySearch(OrderedList list, T target, SharedFound found) {
        this.list = list;
        this.found = found;
        this.found.setFound(false);
        this.target = target;
        semTam = (((int) (list.rear + 1) / SearchStack) + 1);
        sem = new Semaphore(semTam);
    }

    /**
     * Constructor Thread that changes the static element "found"
     *
     * @param elements elements that gonna be search
     * @param target Target element to be search
     * @param sem Semaphore to make wait the first thread for all others
     * @param semTam Amount of resources
     * @param found
     */
    public ThreadBinarySearch(T[] elements, T target, Semaphore sem, int semTam, SharedFound found) {
        this.elements = elements;
        this.target = target;
        this.found = found;
        this.sem = sem;
        this.semTam = semTam;
    }

    /**
     * Runnable of Thread where all magic happen The First Thread calculate the
     * amount of thread that going to be launch, and wait for it to ends This
     * method is recursive so it uses it self to get to the end result.
     */
    @Override
    public void run() {
        if (list != null) {
            int tam = 0;
            for (int i = 0; i * SearchStack < list.rear + 1; i++) {
                if ((i + 1) * SearchStack > list.rear + 1) {
                    tam = (list.rear + 1) - (i * SearchStack);
                } else {
                    tam = SearchStack;
                }
                T[] temp = (T[]) (new Object[tam]);
                System.arraycopy(list.Collection, (i * SearchStack), temp, 0, tam);
                try {
                    sem.acquire();
                    (new Thread(new ThreadBinarySearch(temp, target, sem, semTam, found))).start();
                } catch (InterruptedException ex) {
                }
            }
            try {
                sem.acquire(semTam);
            } catch (InterruptedException ex) {
            }
        } else {
            if (OrderedList.binarySearch(elements, 0, elements.length - 1, target)) {
                found.setFound(true);
            }
            sem.release();
        }
    }
}
