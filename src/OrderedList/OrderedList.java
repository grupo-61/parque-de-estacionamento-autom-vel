package OrderedList;

import OrderedList.Interfaces.*;

/**
 *
 * @author Hugo Teixeira
 * @param <T>
 */
public class OrderedList<T> extends ListADT<T> implements OrderedListADT<T> {

    /**
     * {@inheritDoc }
     */
    @Override
    public void add(T element) {
        if (super.size() == Collection.length) {
            super.Grow();
        }
        Comparable<T> temp = (Comparable) element;
        int i = 0;
        while (i <= rear && temp.compareTo(Collection[i]) > 0) {
            i++;
        }
        for (int scan = (rear + 1); scan > i; scan--) {
            Collection[scan] = Collection[scan - 1];
        }
        Collection[i] = element;
        rear++;
        modCount++;
    }

    /**
     * Searches the specified array of objects using a linear search algorithm.
     *
     * @param <T> Type T element with Comparable implemented
     * @param data the array to be sorted
     * @param min the integer representation of the min value
     * @param max the integer representation of the max value
     * @param target the element being searched for
     * @return true if the desired element is found
     */
    public static <T> boolean binarySearch(T[] data, int min, int max, T target) {
        boolean found = false;
        int midpoint = (min + max) / 2; // determine the midpoint
        Comparable comp = (Comparable) target;
        if (comp.compareTo(data[midpoint]) == 0) {
            found = true;
        } else if (comp.compareTo(data[midpoint]) < 0) {
            if (min <= midpoint - 1) {
                found = binarySearch(data, min, midpoint - 1, target);
            }
        } else if (midpoint + 1 <= max) {
            found = binarySearch(data, midpoint + 1, max, target);
        }
        return found;
    }
}
