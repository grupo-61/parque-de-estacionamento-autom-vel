package OrderedList;

import OrderedList.Interfaces.*;
import java.util.EmptyStackException;
import java.util.Iterator;
import org.omg.CosNaming.NamingContextPackage.NotFound;

/**
 *
 * @author Hugo Teixeira
 * @param <T>
 */
public class ListADT<T> implements ListADT_Interface<T>, Iterable<T> {

    /**
     * Constant´s
     */
    private static final int DEFAULT_CAPACITY = 5;
    private static final int EXPAND_BY = 2;

    /**
     * Element of Class
     */
    protected T[] Collection;
    protected int rear;
    protected int modCount;

    /**
     * Constructor Method
     */
    public ListADT() {
        this.rear = -1;
        this.modCount = 0;
        Collection = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public T removeFirst() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        T removed = Collection[0];
        for (int i = 0; i < rear; i++) {
            Collection[i] = Collection[i + 1];
        }
        Collection[rear--] = null;
        modCount++;
        return removed;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public T removeLast() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        T removed = Collection[rear];
        Collection[rear--] = null;
        modCount++;
        return removed;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public T remove(T x) throws NotFound {

        T removed;
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        if (!contains(x)) {
            throw new NotFound();
        }
        int i = find(x);
        removed = Collection[i];
        Collection[i] = null;
        for (; i < rear; i++) {
            Collection[i] = Collection[i + 1];
        }
        Collection[rear--] = null;
        modCount++;

        return removed;
    }

    /**
     * Method to find a element in Collection
     *
     * @param x element to find
     * @return position of element in array or -1 if not found
     */
    protected int find(T x) {
        int i = -1;
        for (int y = 0; y <= rear && i == -1; y++) {
            if (Collection[y].equals(x)) {
                i = y;
            }
        }
        return i;
    }

    /**
     * Method to expend the Collection
     */
    protected void Grow() {
        T[] newArray = (T[]) (new Object[Collection.length * EXPAND_BY]);
        System.arraycopy(Collection, 0, newArray, 0, Collection.length);
        Collection = newArray;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean contains(T target) {
        return (find(target) != -1);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public T first() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return Collection[0];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public T last() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return Collection[rear];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isEmpty() {
        return (Collection[0] == null);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int size() {
        return (rear + 1);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Iterator<T> iterator() {
        return (new BasicIterator<>());
    }

    /**
     * Method that return a class with a Iterator implemented
     *
     * @param <T> type of elements
     */
    private class BasicIterator<T> implements Iterator<T> {

        private final int expectedModCount;
        private int count;

        public BasicIterator() {
            this.expectedModCount = 0;
            this.count = 0;
        }

        @Override
        public boolean hasNext() {
            return (count <= rear);
        }

        @Override
        public T next() {
            return (T) (Collection[count++]);
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String toString() {
        if (isEmpty()) {
            return ("Is empty");
        }
        String string = "";
        for (int i = 0; i <= rear; i++) {
            string += Collection[i].toString() + "\r\n";
        }
        return string;
    }
}
