package Card.Exceptions;

/**
 * When code have there are other things without being digits
 *
 * @author 8180149
 */
public class CodeLengthException extends Exception {

    /**
     * Creates a new instance of <code>CodeLengthException</code> without detail
     * message.
     */
    public CodeLengthException() {
        System.err.println("Code Length incorrect");
    }

    /**
     * Constructs an instance of <code>CodeLengthException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public CodeLengthException(String msg) {
        super(msg);
    }
}
