package Card.Exceptions;

/**
 * When code have number of digits wrong
 *
 * @author 8180149
 */
public class CodeHasNotNumbersException extends Exception {

    /**
     * Creates a new instance of <code>CodeHasNotNumbersException</code> without
     * detail message.
     */
    public CodeHasNotNumbersException() {
        System.err.println("Code not has only numbers");
    }

    /**
     * Constructs an instance of <code>CodeHasNotNumbersException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public CodeHasNotNumbersException(String msg) {
        super(msg);
    }
}
