package Card;

import Card.Exceptions.*;

/**
 *
 * @author 8180149
 */
public final class Card implements Comparable<Card> {

    /**
     * Primitive to store the card code
     */
    private String code;

    /**
     * Constructor Method to create a card
     *
     * @param code card code
     * @throws CodeLengthException When code have number of digits wrong
     * @throws CodeHasNotNumbersException When code have there are other things
     * without being digits
     */
    public Card(String code) throws CodeLengthException, CodeHasNotNumbersException {
        setCode(code);
    }

    /**
     * Method to compare with other cards
     *
     * @param o Card Target
     * @return Difference between both
     */
    @Override
    public int compareTo(Card o) {
        return code.compareTo(o.getCode());
    }

    /**
     * Method to get the code
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Method to set a code
     *
     * @param code the code to set
     * @throws Card.Exceptions.CodeLengthException When code have number of
     * digits wrong
     * @throws Card.Exceptions.CodeHasNotNumbersException When code have there
     * are other things without being digits
     */
    public void setCode(String code) throws CodeLengthException, CodeHasNotNumbersException {
        if (code.length() != 4) {
            throw new CodeLengthException();
        }
        for (char c : code.toCharArray()) {
            if (!Character.isDigit(c)) {
                throw new CodeHasNotNumbersException();
            }
        }
        this.code = code;
    }

    /**
     * {@inheritDoc }
     * @return String that represents the card
     */
    @Override
    public String toString() {
        return code;
    }
}
