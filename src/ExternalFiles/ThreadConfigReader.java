package ExternalFiles;

import SharedObjects.SharedLoadedFiles;
import Card.Card;
import Card.Exceptions.CodeHasNotNumbersException;
import Card.Exceptions.CodeLengthException;
import OrderedList.OrderedList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author 8180149
 */
public class ThreadConfigReader implements Runnable {

    /**
     * Shared file where is loaded all information
     */
    private final SharedLoadedFiles file;

    /**
     * Constructor Method to get Shared Files
     *
     * @param file SharedLoadedFiles
     */
    public ThreadConfigReader(SharedLoadedFiles file) {
        this.file = file;
    }

    /**
     * Thread that reads all Configuration from file ".txt" and load it to the system
     */
    @Override
    public void run() {
        FileReader read;
        FileWriter writer;
        try {
            read = new FileReader("configuration.txt");
            BufferedReader buffer = new BufferedReader(read);
            String[] cell = buffer.readLine().split(" = ");
            OrderedList<Card> tempCards = new OrderedList();
            buffer.readLine();
            String str;
            while ((str = buffer.readLine()) != null) {
                tempCards.add(new Card(str));
            }
            file.setParkingSlots(Integer.parseInt(cell[1]));
            file.setCards(tempCards);
        } catch (CodeHasNotNumbersException | CodeLengthException | IOException | ArrayIndexOutOfBoundsException ie) {
            System.err.println("Error, Configuration File");
            try {
                //Não faz sentido

                writer = new FileWriter("configuration.txt", false);
                writer.write("parkingSlots = " + file.getParkingSlots() + "\r\n");
                writer.write("Cards : \r\n");
                writer.write(file.getCards().toString());
                writer.close();
            } catch (IOException ex) {
                System.err.println("Error Creating Configuration File");
            }
        }
    }
}
