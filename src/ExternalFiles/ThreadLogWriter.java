package ExternalFiles;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author 8180149
 */
public class ThreadLogWriter implements Runnable {

    /**
     * Primitive that is given to write on the "Log.txt"
     */
    private final String text;

    /**
     * Constant limiting the log file to not be too large
     */
    private final int LINES = 200000;

    /**
     * Constructor method
     * @param text 
     */
    public ThreadLogWriter(String text) {
        this.text = text;
    }

    /**
     * Thread that write on the file ".txt"
     */
    @Override
    public void run() {
        FileReader read;
        FileWriter writer;
        boolean reset = false;

        try {
            read = new FileReader("Log.txt");
            BufferedReader buffer = new BufferedReader(read);

            if (buffer.lines().count() > LINES) {
                reset = true;
            }

            read.close();
            writer = new FileWriter("Log.txt", !reset);
            writer.write(text + "\r\n");
            writer.close();
        } catch (FileNotFoundException ie) {
            try {
                writer = new FileWriter("Log.txt", !reset);
                writer.write(text + "\r\n");
                writer.close();
            } catch (IOException ex) {
                System.err.println("Error, Log File");
            }
            System.err.println("File was created");
        } catch (IOException e) {

            System.err.println("Error, Log File+");
        }
    }
}
