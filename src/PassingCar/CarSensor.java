package PassingCar;

/**
 * Class represents Sensor
 * @author 8180149
 */
public class CarSensor {
    /**
     * Static Primitive that say if car is passing or not 
     */
    private static boolean passingCar = false;

    /**
     * Return if car is passing or not
     * @return the passingCar
     */
    public static boolean isPassingCar() {
        return passingCar;
    }

    /**
     * Sets car passing or not
     * @param aPassingCar the passingCar to set
     */
    public static void setPassingCar(boolean aPassingCar) {
        passingCar = aPassingCar;
    }
}
