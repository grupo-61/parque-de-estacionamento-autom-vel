package PassingCar;

import GraphicInterfaces.SemaphoreInterface;
import Semaforo.Semaforo;
import Semaforo.ThreadSemaforo;
import java.util.concurrent.Semaphore;

/**
 * Thread that simulates car passing
 *
 * @author 8180149
 */
public class ThreadCarSensor extends Thread {

    /**
     * Permits to enter and leave
     */
    private final Semaphore EnterLeave;

    /**
     * Label Status of sensor
     */
    private final javax.swing.JLabel sensor;

    /**
     * Thread to update Semaphore
     */
    private final Thread Update;

    /**
     * Label of parking slots avaliable
     */
    private final javax.swing.JLabel ParkingSlots;

    /**
     * Number of cars in the system
     */
    private final int cars;

    /**
     * Number of parking slots in the system
     */
    private final int parkingSlots;

    /**
     * Constructor Method of Threads
     *
     * @param EnterLeave
     * @param sensor
     * @param parkingSlots
     * @param cars
     * @param SemInt
     * @param ParkingSlots
     */
    public ThreadCarSensor(Semaphore EnterLeave, javax.swing.JLabel sensor, int parkingSlots, int cars, SemaphoreInterface SemInt, javax.swing.JLabel ParkingSlots) {
        this.sensor = sensor;
        this.EnterLeave = EnterLeave;
        this.cars = cars;
        this.parkingSlots = parkingSlots;
        this.Update = new Thread(new ThreadSemaforo(parkingSlots, cars, SemInt));
        this.ParkingSlots = ParkingSlots;
    }

    /**
     * Runnable of Thread
     */
    @Override
    public void run() {
        //Sets Sensor passing
        CarSensor.setPassingCar(true);
        
        try {
            //Consume permit to Enter or Leave
            EnterLeave.acquire();
            
            //Information to the user
            sensor.setText("Passing");
            sensor.setEnabled(true);
            
            //Car time is passing
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        
        //Sets Sensor off
        CarSensor.setPassingCar(false);
        
        //If system is freeze the status text dont change
        if (!sensor.getText().equals("Stop")) {
            sensor.setText("none");
            sensor.setEnabled(false);
        }
        
        //Update Semaphore if system isnt frozen
        if (!Semaforo.isForcedRed()) {
            Update.start();
        }
        //Update status parking slots to the user
        ParkingSlots.setText(String.valueOf(this.parkingSlots - this.cars));
    }
}
