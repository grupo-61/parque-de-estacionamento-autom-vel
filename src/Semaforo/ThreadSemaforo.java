package Semaforo;

import GraphicInterfaces.SemaphoreInterface;

/**
 * Thread that Update Semaphore
 *
 * @author 8180149
 */
public class ThreadSemaforo implements Runnable {

    /**
     * Primitive of parking slots
     */
    private int parkingSlots;

    /**
     * Number of cars on the system
     */
    private int cars;

    /**
     * If turnRed or not the Semaphore
     */
    private final boolean turnRed;

    /**
     * Semaphore Graphic Interface
     */
    private final SemaphoreInterface SemInt;

    /**
     * Constructor Method to Update Semaphore
     *
     * @param parkingSlots
     * @param cars
     * @param SemInt
     */
    public ThreadSemaforo(int parkingSlots, int cars, SemaphoreInterface SemInt) {
        this.parkingSlots = parkingSlots;
        this.cars = cars;
        this.SemInt = SemInt;
        this.turnRed = false;
    }

    /**
     * Constructor Method to forcibly change color
     *
     * @param turnRed
     * @param SemInt
     */
    public ThreadSemaforo(boolean turnRed, SemaphoreInterface SemInt) {
        this.turnRed = true;
        this.SemInt = SemInt;
    }

    /**
     * Runnable of Thread
     */
    @Override
    public void run() {
        if (!turnRed) {
            //Semaphore color update
            Semaforo.setForcedRed(false);
            if (this.cars < this.parkingSlots) {
                Semaforo.setIsGreen(true);
                SemInt.setGreen();
            } else {
                SemInt.setRed();
                Semaforo.setIsGreen(false);
            }
        } else {
            //Turn Semaphore Red
            SemInt.setRed();
            Semaforo.setIsGreen(false);
            Semaforo.setForcedRed(true);
        }
    }
}
