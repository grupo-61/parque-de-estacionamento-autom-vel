package Semaforo;

/**
 *
 * @author 8180149
 */
public class Semaforo {

    /**
     * If Semaphore is green
     */
    private static boolean isGreen;

    /**
     * If as a forcedRed (system stop active)
     */
    private static boolean forcedRed;

    public Semaforo() {
    }

    /**
     * Method that returns if is forced red or not
     *
     * @return the forcedRed
     */
    public static boolean isForcedRed() {
        return forcedRed;
    }

    /**
     * Method that sets a forced Red
     *
     * @param aForcedRed the forcedRed to set
     */
    public static void setForcedRed(boolean aForcedRed) {
        forcedRed = aForcedRed;
    }

    /**
     * Return if is green or not
     *
     * @return
     */
    public static boolean getIsGreen() {
        return isGreen;
    }

    /**
     * Sets the Semaphore to green or not
     * @param aIsGreen
     */
    public static void setIsGreen(boolean aIsGreen) {
        isGreen = aIsGreen;
    }
}
