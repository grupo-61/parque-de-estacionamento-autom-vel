package Gate;

import ExternalFiles.ThreadLogWriter;
import PassingCar.CarSensor;
import java.util.Date;
import java.util.concurrent.Semaphore;

/**
 *
 * @author 8180149
 */
public class ThreadOpenGateWithTimer implements Runnable {

    /**
     * Timer for Gate being open
     */
    private int timer;
    private final Gate gate;

    /**
     * Object that have the permits to enter and leave
     */
    private final Semaphore EnterLeave;

    /**
     * Monitor that stop the Thread when system is stopped
     */
    private final Monitor mn;

    /**
     * Construct Method that receives all information needed
     *
     * @param timer
     * @param gate
     * @param EnterLeave
     * @param mn
     */
    public ThreadOpenGateWithTimer(int timer, Gate gate, Semaphore EnterLeave, Monitor mn) {
        this.timer = timer;
        this.gate = gate;
        this.EnterLeave = EnterLeave;
        this.mn = mn;
    }

    /**
     * Thread witch control the opening and closing of the gate
     */
    @Override
    public void run() {

        //Set gate open
        gate.setOpen(true);

        //Give permit to one car enter
        EnterLeave.release();

        //Writes in Log
        (new Thread(new ThreadLogWriter(new Date().toString() + " Opening Auto Gate"))).start();

        //Timer of gate being open
        for (int i = 0; i < timer; i++) {
            try {

                //Set timer to 3 secs after car pass
                if (CarSensor.isPassingCar()) {
                    i = 0;
                    timer = 3;
                }

                //Gives car time to pass
                do {
                    Thread.sleep(500);
                } while (CarSensor.isPassingCar());

                //Frezzes thread if system is stopped
                if (mn.isUsing()) {
                    mn.Threadsleep();
                }
            } catch (InterruptedException ex) {
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
            }
        }

        //Set gate Closed
        gate.setOpen(false);

        //Write in the log
        (new Thread(new ThreadLogWriter(new Date().toString() + " Closing Auto Gate"))).start();

        //Removes one permit to enter
        if (EnterLeave.availablePermits() > 0) {
            try {
                EnterLeave.acquire();
            } catch (InterruptedException ex) {
            }
        }
    }
}
