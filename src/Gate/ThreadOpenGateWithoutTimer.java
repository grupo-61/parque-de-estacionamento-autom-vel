package Gate;

/**
 * Thread to open or close manually the gate (Without timer)
 *
 * @author joaop
 */
public class ThreadOpenGateWithoutTimer implements Runnable {

    /**
     * Primitive that set´s gate open or closed
     */
    private final boolean getOpen;

    /**
     * Object gate
     */
    private final Gate gate;

    /**
     * Constructor Method
     *
     * @param setOpen
     * @param gate
     */
    public ThreadOpenGateWithoutTimer(boolean setOpen, Gate gate) {
        this.getOpen = setOpen;
        this.gate = gate;
    }

    /**
     * Runnable of Thread
     */
    @Override
    public void run() {
        gate.setOpen(getOpen);
    }
}
