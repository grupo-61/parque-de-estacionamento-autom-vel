package Gate;

import GraphicInterfaces.GateInterface;
import javax.swing.JLabel;

/**
 *
 * @author 8180149
 */
public class Gate {

    /**
     * Primitive that is static because is only one
     */
    private boolean Open;

    /**
     * Object of gate Status
     */
    private final javax.swing.JLabel gateStatus;

    /**
     * Gate Interface
     */
    private final GateInterface GateInter;

    /**
     * Constructor Method
     *
     * @param gateStatus
     * @param GateInter
     */
    public Gate(JLabel gateStatus, GateInterface GateInter) {
        this.gateStatus = gateStatus;
        this.Open = false;
        this.GateInter = GateInter;
    }

    /**
     * Return is Gate is open(true) or Closed(false)
     *
     * @return the isOpen
     */
    public boolean isOpen() {
        return Open;
    }

    /**
     * Method to set gate closed(false) or open(true)
     *
     * @param aIsOpen the isOpen to set
     */
    public void setOpen(boolean aIsOpen) {
        Open = aIsOpen;
        if (Open) {
            gateStatus.setText("Open");
            gateStatus.setEnabled(true);
            GateInter.setGateOpen();
        } else {
            gateStatus.setText("Closed");
            gateStatus.setEnabled(false);
            GateInter.setGateClose();
        }
    }

}
