package Gate;

/**
 *
 * @author 8180149
 */
public class Monitor {

    /**
     * Primitive to inform if Object is being used.
     */
    private boolean using;

    /**
     * Constructor method
     */
    public Monitor() {
        this.using = false;
    }

    /**
     * Method that wake up all Thread in wait
     */
    public synchronized void wakeUp() {
        this.notify();
        this.setUsing(false);
    }

    /**
     * Method that put Thread in wait
     */
    public synchronized void Threadsleep() {
        try {
            this.wait();
            this.setUsing(false);
        } catch (InterruptedException ex) {
        }
    }

    /**
     * Method that return is object is being used or not
     *
     * @return the using
     */
    public boolean isUsing() {
        return using;
    }

    /**
     * Method that set´s that object is being used.
     *
     * @param using the using to set
     */
    public void setUsing(boolean using) {
        this.using = using;
    }
}
