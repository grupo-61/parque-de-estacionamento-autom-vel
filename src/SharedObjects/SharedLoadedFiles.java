package SharedObjects;

import Card.Card;
import Card.Exceptions.CodeHasNotNumbersException;
import Card.Exceptions.CodeLengthException;
import OrderedList.OrderedList;

/**
 *
 * @author 8180149
 */
public class SharedLoadedFiles {

    /**
     * Constant of parking slots by default
     */
    private final int PARKINGSLOTSDEFAULT = 50;

    /**
     * List of cards
     */
    private OrderedList<Card> cards;

    /**
     * Number of parkingSlots
     */
    private int parkingSlots;

    /**
     * Constructor method
     *
     * @throws CodeLengthException
     * @throws CodeHasNotNumbersException
     */
    public SharedLoadedFiles() throws CodeLengthException, CodeHasNotNumbersException {
        parkingSlots = PARKINGSLOTSDEFAULT;
        cards = new OrderedList<>();
        cards.add(new Card("1111"));
        cards.add(new Card("2222"));
        cards.add(new Card("3333"));
        cards.add(new Card("4444"));
        cards.add(new Card("5555"));
    }

    /**
     * Method that returns cards
     *
     * @return the cards
     */
    public OrderedList<Card> getCards() {
        return cards;
    }

    /**
     * Method that set Cards
     *
     * @param cards the cards to set
     */
    public void setCards(OrderedList<Card> cards) {
        this.cards = cards;
    }

    /**
     * Method that get number of parking slots
     *
     * @return the parkingSlots
     */
    public int getParkingSlots() {
        return parkingSlots;
    }

    /**
     * Method that sets number of parking slots
     *
     * @param parkingSlots the parkingSlots to set
     */
    public void setParkingSlots(int parkingSlots) {
        this.parkingSlots = parkingSlots;
    }

}
