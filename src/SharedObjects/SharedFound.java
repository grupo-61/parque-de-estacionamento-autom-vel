package SharedObjects;

/**
 *
 * @author 8180149
 */
public class SharedFound {

    /**
     * Primitive that keep if was found or not
     */
    private boolean found;

    /**
     * Method that return found
     *
     * @return the found
     */
    public boolean isFound() {
        return found;
    }

    /**
     * Method that sets found
     *
     * @param found the found to set
     */
    public void setFound(boolean found) {
        this.found = found;
    }

}
